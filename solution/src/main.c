#include "bmp_util.h"
#include "image_util.h"
#include "transform_util.h"

#include <stdio.h>
#include <stdlib.h>



#define EXIT_SUCCESS 0
#define EXIT_FAILURE 1
#define FULL_TURNOVER 360

int normalize_angle(long angle) {
    int normalized_angle = (int)(angle % FULL_TURNOVER);
    if (normalized_angle <= 0) {
        normalized_angle += FULL_TURNOVER;
    }
    return normalized_angle;
}

int main(int argc, char* argv[]) {
    if (argc != 4) {
        fprintf(stderr, "Usage: %s <source-image> <transformed-image> <angle>\n", argv[0]);
        return EXIT_FAILURE;
    }

    char* endptr;
    long angle = strtol(argv[3], &endptr, 10);
    angle = normalize_angle(angle);

    if (*endptr != '\0') {
        fprintf(stderr, "Error: Invalid angle input\n");
        return EXIT_FAILURE;
    }
    if (angle % 90 != 0) {
        fprintf(stderr, "Error: Angle must be a multiple of 90 degrees.\n");
        return EXIT_FAILURE;
    }

    struct image source_image;
    FILE* source_file = fopen(argv[1], "rb");
    if (!source_file) {
        perror("Error opening source image file");
        return EXIT_FAILURE;
    }

    enum read_status read_status = from_bmp(source_file, &source_image);
    fclose(source_file);

    if (read_status != READ_OK) {
        fprintf(stderr, "Error reading source image: %d\n", read_status);
        de_init_image(&source_image);
        return EXIT_FAILURE;
    }

    struct image temp_image = source_image;

    for (int8_t i = 0; i < (int8_t)(angle / 90); i++) {
        struct image new_image = rotate_image(&temp_image);

        if (temp_image.data != NULL) {
            free(temp_image.data);
            temp_image.data = NULL;
        }

        temp_image = new_image;
        new_image.data = NULL;
    }

    FILE* transformed_file = fopen(argv[2], "wb");
    if (!transformed_file) {
        de_init_image(&temp_image);
        perror("Error opening transformed image file");
        return EXIT_FAILURE;
    }

    enum write_status write_status = to_bmp(transformed_file, &temp_image);
    fclose(transformed_file);

    if (write_status != WRITE_OK) {
        de_init_image(&temp_image);
        fprintf(stderr, "Error writing transformed image: %d\n", write_status);
        return EXIT_FAILURE;
    }

    printf("Transformation successful.\n");

    if (temp_image.data != NULL) {
        free(temp_image.data);
        temp_image.data = NULL;
    }

    return EXIT_SUCCESS;
}

