#include "transform_util.h"

#include <stdlib.h>
#include <string.h>



struct image rotate_image(const struct image* image) {
    struct image output;
    output.width = image->height;
    output.height = image->width;
    output.data = malloc(sizeof(struct pixel) * output.width * output.height);
    if (output.data == NULL) return (struct image) {.height = 0, .width = 0, .data = NULL};

    for (size_t i = 0; i < output.width; i++) {
        for (size_t j = 0; j < output.height; j++) {
            output.data[(output.height - (j + 1)) * output.width + i] = image->data[i * image->width + j];
        }
    }

    return output;
}



