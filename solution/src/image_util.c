#include "image_util.h"

#include <stdlib.h>



void init_image(struct image* img, uint64_t width, uint64_t height) {
    img->width = width;
    img->height = height;
    img->data = malloc(width * height * sizeof(struct pixel));
}


void de_init_image(struct image* img) {
    free(img->data);
}


