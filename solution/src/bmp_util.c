#include "bmp_util.h"
#include <string.h>

#define FILE_MAGIC_NUMBER_BM 0x4D42
#define BITS_PER_PIXEL 24
#define BMP_HEADER_SIZE 40

uint32_t calculate_padding(uint32_t width) {
    return (4 - (width * sizeof(struct pixel)) % 4) % 4;
}

enum read_status from_bmp(FILE* in, struct image* img) {
    struct bmp_header header;
    if (fread(&header, sizeof(struct bmp_header), 1, in) != 1) {
        return READ_IO_ERROR;
    }

    if (header.bfType != 0x4D42) {
        return READ_INVALID_SIGNATURE;
    }

    init_image(img, header.biWidth, header.biHeight);

    for (uint64_t i = 0; i < img->height; ++i) {
        if (fread(img->data + i * img->width, sizeof(struct pixel), img->width, in) != img->width) {
            return READ_IO_ERROR;
        }
        if (fseek(in, calculate_padding(img->width), SEEK_CUR) != 0) {
            return READ_IO_ERROR;
        }
    }

    return READ_OK;
}

enum write_status to_bmp(FILE* out, const struct image* img) {
    struct bmp_header header;
    header.bfType = FILE_MAGIC_NUMBER_BM;
    header.bfileSize = sizeof(struct bmp_header) + img->width * img->height * sizeof(struct pixel) * 2;
    header.bfReserved = 0;
    header.bOffBits = sizeof(struct bmp_header);
    header.biSize = BMP_HEADER_SIZE;
    header.biWidth = img->width;
    header.biHeight = img->height;
    header.biPlanes = 1;
    header.biBitCount = BITS_PER_PIXEL; // 24 бита на пиксель (3 байта)
    header.biCompression = 0;
    header.biSizeImage = 0;
    header.biXPelsPerMeter = 0;
    header.biYPelsPerMeter = 0;
    header.biClrUsed = 0;
    header.biClrImportant = 0;

    fwrite(&header, sizeof(struct bmp_header), 1, out);

    for (uint64_t i = 0; i < img->height; ++i) {
        fwrite(img->data + i * img->width, sizeof(struct pixel), img->width, out);

        uint32_t padding = calculate_padding(img->width);
        uint8_t padBytes[padding];
        memset(padBytes, 0, padding);
        fwrite(padBytes, 1, padding, out);
    }

    return WRITE_OK;
}
