#ifndef TRANSFORM_UTIL_H
#define TRANSFORM_UTIL_H

#include "image_util.h"

struct image rotate_image(const struct image* image);

#endif

